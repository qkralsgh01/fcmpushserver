package com.jkss.fcm.controller;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jkss.fcm.service.PushService;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/sendMessage")
public class PushMessageController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private PushService pushService;
	
	@PostMapping("/topic")
    public ResponseEntity<Resource> messageForTopic(
    			HttpServletRequest request, 
    			@RequestBody Map<String,String> paramMap
    		) throws IOException {
		
		logger.info("sendMessage paramMap : " + paramMap);
		
		String topic = paramMap.get("topic");
		String title = paramMap.get("title");
		String message = paramMap.get("message");
		String url = paramMap.get("url");
		pushService.pushAlarmToTopic(title, message, topic, url);

        // ResponseEntity 객체 생성 및 반환
        return ResponseEntity.ok().build();
    }
	
	
	@PostMapping("/token")
    public ResponseEntity<Resource> messageForIndivisual(
    			HttpServletRequest request, 
    			@RequestBody Map<String,String> paramMap
    		) throws IOException {
		
		logger.info("sendMessage paramMap : " + paramMap);
		
		String token = paramMap.get("token");
		String title = paramMap.get("title");
		String message = paramMap.get("message");
		String url = paramMap.get("url");
		if(url == null ) {
			url = "";
		}
		pushService.pushAlarmToToken(title, message, token, url);

        // ResponseEntity 객체 생성 및 반환
        return ResponseEntity.ok().build();
    }
}
