package com.jkss.fcm.v1.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.jkss.fcm.v1.domain.SendTokenDTO;
import com.jkss.fcm.v1.domain.SendTopicDTO;
import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
@Slf4j
public class FcmPushService {

    private static final String FILE_PATH = "C:/Users/Administrator/Desktop/api/conf/";

    /**
     * 인증정보 초기화
     * @param fileName 파일명
     */
    private void FirebaseOptionsInit(String fileName) throws IOException {
        FirebaseApp.getInstance().delete();
        FirebaseOptions firebaseOptions = FirebaseOptions.builder()
                .setCredentials(GoogleCredentials
                        .fromStream(new FileInputStream(FILE_PATH + fileName))
                        .createScoped(List.of("https://www.googleapis.com/auth/cloud-platform"))
                ).build();

        FirebaseApp.initializeApp(firebaseOptions);
    }

    /**
     * 메세지 전송
     * @param message 메세지
     */
    private void sendMessage(Message message) throws ExecutionException, InterruptedException {
        String response = FirebaseMessaging.getInstance().sendAsync(message).get();
        log.debug("##### SendMessage response : {}", response);
    }

    /**
     * 토픽메세지 전송
     * @param sendTopicDTO topic 요청정보
     */
    public void pushAlarmToTopic(SendTopicDTO sendTopicDTO) throws IOException, ExecutionException, InterruptedException {
        String topic        = sendTopicDTO.getTopic();
        String title        = sendTopicDTO.getTitle();;
        String messageBody  = sendTopicDTO.getMessage();
        String url          = sendTopicDTO.getUrl();
        String fileName     = sendTopicDTO.getFileName();

        if(StringUtils.isEmpty(topic)){
            throw new RuntimeException("##### Topic is empty");
        }
        if(StringUtils.isEmpty(fileName)){
            throw new RuntimeException("##### FileName is empty");
        }

        FirebaseOptionsInit(fileName);

        Notification notification = Notification.builder().setTitle(title).setBody(messageBody).build();

        Message message = Message.builder()
                .setTopic(topic)
                .setNotification(notification)
                .putData("url", url)
                .build();
        sendMessage(message);
    }

    /**
     * 토큰메세지 전송
     * @param sendTokenDTO token 요청정보
     */
    public void pushAlarmToToken(SendTokenDTO sendTokenDTO) throws IOException, ExecutionException, InterruptedException {
        String token        = sendTokenDTO.getToken();
        String title        = sendTokenDTO.getTitle();;
        String messageBody  = sendTokenDTO.getMessage();
        String url          = sendTokenDTO.getUrl();
        String fileName     = sendTokenDTO.getFileName();

        if(StringUtils.isEmpty(token)){
            throw new RuntimeException("##### Token is empty");
        }
        if(StringUtils.isEmpty(fileName)){
            throw new RuntimeException("##### FileName is empty");
        }

        FirebaseOptionsInit(fileName);

        Notification notification = Notification.builder().setTitle(title).setBody(messageBody).build();

        Message message = Message.builder()
                .setToken(token)
                .setNotification(notification)
                .putData("url", url)
                .build();
        sendMessage(message);
    }


}
