package com.jkss.fcm.v1.controller;

import com.jkss.fcm.v1.domain.SendTokenDTO;
import com.jkss.fcm.v1.domain.SendTopicDTO;
import com.jkss.fcm.v1.service.FcmPushService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sendMessage")
@Slf4j
public class FcmPushMessageController {

    private final FcmPushService fcmPushService;

    @RequestMapping("/topic")
    public Map<String, Object> sendMessageTopic(@RequestBody SendTopicDTO sendTopicDTO){
        log.debug("##### SendTopicDTO : {}", sendTopicDTO.toString());
        final Map<String, Object> responseMap = new HashMap<>();
        try{
            fcmPushService.pushAlarmToTopic(sendTopicDTO);
            responseMap.put("resultCode", 1);
        }catch (Exception e){
            log.debug("##### SendMessageTopic Exception ", e);
            responseMap.put("resultCode", 0);
        }
        return responseMap;
    }

    @RequestMapping("/token")
    public Map<String, Object> sendMessageToken(@RequestBody SendTokenDTO sendTokenDTO){
        log.debug("##### SendTokenDTO : {}", sendTokenDTO.toString());
        final Map<String, Object> responseMap = new HashMap<>();
        try{
            fcmPushService.pushAlarmToToken(sendTokenDTO);
            responseMap.put("resultCode", 1);
        }catch (Exception e){
            log.debug("##### SendMessageToken Exception ", e);
            responseMap.put("resultCode", 0);
        }
        return responseMap;
    }
}
