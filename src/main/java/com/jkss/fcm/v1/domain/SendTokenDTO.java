package com.jkss.fcm.v1.domain;

import lombok.Data;

@Data
public class SendTokenDTO {

    private String token;

    private String title;

    private String message;

    private String url;

    private String fileName;
}
