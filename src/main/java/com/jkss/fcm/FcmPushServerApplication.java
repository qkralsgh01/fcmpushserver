package com.jkss.fcm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class FcmPushServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FcmPushServerApplication.class, args);
	}

}
