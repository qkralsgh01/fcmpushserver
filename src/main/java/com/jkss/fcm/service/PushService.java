package com.jkss.fcm.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;


@Service
public class PushService {

	private String FCM_PRIVATE_KEY_PATH = "firebase/purmee-voucher-card-be2a49f865d1.json";
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	//푸시인증서 기반으로 초기화
	@PostConstruct
    public void init() {
        try {

			FirebaseOptions options = FirebaseOptions.builder()
                    .setCredentials(
                            GoogleCredentials
                                    .fromStream(new ClassPathResource(FCM_PRIVATE_KEY_PATH).getInputStream())
                                    .createScoped(List.of("https://www.googleapis.com/auth/cloud-platform")))
                    .build();
            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
               
            }
        } catch (IOException e) {
            e.printStackTrace();
            // spring 뜰때 알림 서버가 잘 동작하지 않는 것이므로 바로 죽임
            throw new RuntimeException(e.getMessage());
        }
    }
	
	//개별토큰메시지 구성
	@Async
	public void pushAlarmToToken(String title, String messageBody, String token, String url) {
        if (token != null) {
            Notification notification = Notification.builder().setTitle(title).setBody(messageBody).build();
            Message message = Message.builder()
            	    .setToken(token)
            	    .setNotification(notification)
            	    .putData("url", url)
            	    .build();
            
            sendMessage(message);
        }
    }
	
	//토픽메시지 구성
	@Async
	public void pushAlarmToTopic(String title, String messageBody, String topic, String url) {
        if (topic != null) {
            Notification notification = Notification.builder().setTitle(title).setBody(messageBody).build();


            Message message = Message.builder()
            	    .setTopic(topic)
            	    .setNotification(notification)
            	    .putData("url", url)
            	    .build();
            sendMessage(message);
        }
    }
	
	//메세지 전송 비동기적으로 처리
	@Async
    public void sendMessage(Message message) {
        try {
            String response = FirebaseMessaging.getInstance().sendAsync(message).get();

            logger.info("전송 성공 : " + response);
        } catch (ExecutionException | InterruptedException e) {
        	logger.info("실패 : " + e.getMessage());
        }
    }
}
